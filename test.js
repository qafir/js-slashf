const path = require('path');
const { slashf, slash } = require('.');

/* eslint-env jest */

test('Wrapped functions', () => {
  expect(
    slashf(
      (one, two, three) => `${one}\\${two}/${three}.something`,
    )('hey', 'you', 'babe'),
  ).toBe('hey/you/babe.something');

  expect(
    slashf(path.join)('blah', 'something'),
  ).toBe('blah/something');

  expect(
    slashf(
      () => ({ a: 'a\\b' }),
    )(),
  ).toEqual({ a: 'a\\b' });
});

test('Text normalization', () => {
  expect(
    slash({ a: '\\a\\b' })
  ).toEqual({ a: '\\a\\b' });

  expect(
    slash(null)
  ).toBe(null);

  expect(
    slash('/a\\b/c')
  ).toBe('/a/b/c');
});
