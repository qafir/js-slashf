# slashf: forward-slash normalization via composition

This package provides a `slashf(f)` function used to normalize the output of any wrapped function, in the situation in which a string is returned and contains backslashes.

Example usage:

```javascript
import { slashf } from 'slashf';

function myPath(myFileName) {
    return '\\my\\path\\' + myFileName;
}

const myPathNorm = slashf(myPath);

console.log(myPathNorm('myfile.txt'));
```

This snippet would print:

    '/my/path/myfile.txt'

---

## API

### slashf(f)
Wraps a function, normalizes its output - if it's a string - and returns it.

### slash(x)
Normalizes `x` by replacing all the slashes with forward slashes and returns it. If `x` is not a string it's returned as is.

#### Example
```javascript
slash('/a\\b/c') == '/a/b/c';
slash([1, 2, 3]) == [1, 2, 3];
```

## License
This package is distributed under the MIT license. See the *LICENSE* file for further information.
