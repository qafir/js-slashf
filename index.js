/*
  @license
  Copyright © 2018    Alfredo Mungo <alfredo.mungo@protonmail.ch>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the “Software”), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
*/

/*
  Wrapper function replacing all backslashes with forward slashes.

  @param {Function} f - The wrapped function

  @returns A composite function that returns the value of the wrapped function
  with slashes normalized to forward slashes. If the type of the wrapped
  function's returned value is not a string, the result is returned verbatim.
*/
function slashf(f) {
  return (...args) => {
    return slash(
      f(...args)
    );
  };
}

/*
  Normalizes a given path's style by converting all slashes to forward slashes.

  @param {String} x - The input path

  @returns A forward-slash-only version of the input path. If the input path is
    not a string, the result is returned verbatim.
*/
function slash(x) {
  let y;

  if (typeof x === 'string') y = x.replace(/\\/g, '/');
  else y = x;

  return y;
}

module.exports = {
  slashf,
  slash
};
